// Make a class for the handler
const axios = require('axios');
class CryptoPricingHandler {
	async getCryptoPrice(crypto, currency) {
		try {
			const cryptoPrice = await axios.get(`https://api.coingecko.com/api/v3/simple/price?ids=${crypto}&vs_currencies=${currency}`);
			return cryptoPrice.data;
		} catch (error) {
			return error;
		}
	}
}

module.exports = CryptoPricingHandler;

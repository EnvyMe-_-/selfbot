// Funny spotify shit, fuck this api :D

const axios = require('axios');
const {base64EncodedCredentials, refreshToken} = require('../config.json');

class SpotifyHandler {
	constructor() {
		this.spotifyToken = '';
	}

	async refreshSpotifyToken() {
		try {
			// Using axios make a request to https://accounts.spotify.com/api/token with headers and data
			const headers = {
				'Content-Type': 'application/x-www-form-urlencoded',
				Authorization: `Basic ${base64EncodedCredentials}`,
			};

			const data = {
				grant_type: 'refresh_token',
				refresh_token: refreshToken,
			};

			// Send post request with body and headers
			const response = await axios.post('https://accounts.spotify.com/api/token', data, {headers});

			this.spotifyToken = response.data.access_token;

			return response.data.access_token;
		} catch (error) {
			console.log(error.response);
		}
	}

	async getCurrentSongInfo() {
		// Make a request to spotify to get the current song
		// return the current song
		try {
			const headers = {
				Accept: 'application/json',
				'Content-Type': 'application/json',
				Authorization: `Bearer ${this.spotifyToken}`,
			};

			const response = await axios.get('https://api.spotify.com/v1/me/player/currently-playing', {headers});
			return response.data;
		} catch (error) {
			if (error.response.status === 400 || error.response.status === 401) {
				await this.refreshSpotifyToken();
				return this.getCurrentSongInfo();
			}

			if (error.response.status === 204) {
				return 'Nothing is playing!';
			}
		}
	}
}

module.exports = SpotifyHandler;

# selfbot
This is a selfbot made in Node.js using a fork of [Discord.js](https://github.com/aiko-chan-ai/discord.js-selfbot-v13)

## How to use
1. Install [Node.js](https://nodejs.org/en/download/)
2. Clone this repository `git clone https://gitlab.com/EnvyMe-_-/selfbot.git`
3. Install the dependencies `npm install`
4. Rename `example.json` to `config.json`
5. Edit the config.json file with your discord token, spotify refresh token, and your spotify client id and secret separated by a colon `clientid:clientsecret` encode it in base64 then place in the config.json
6. Run the bot with `node index.js`

Note: [This guide](https://benwiz.com/blog/create-spotify-refresh-token/) has a pretty good explanation on getting your refresh token I recommend following it.

## Commands
- `crypto` - Get the price of a cryptocurrency
- `ftoc` - Convert fahrenheit to celsius
- `help` - Get a list of commands
- `load` - Load a command file (useful for loading new commands w/o restarting the bot)
- `reload` - Reloads specified command
- `spotify` - Get the current song you're listening to on spotify
- `uptime` - Get the uptime of the bot

## Credits
- [EnvyMe](https://gitlab.com/EnvyMe-_-) - Main developer
- [aiko-chan-ai](https://github.com/aiko-chan-ai/) - Discord.js fork
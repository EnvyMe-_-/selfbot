const clc = require('cli-color');
module.exports = {
	name: 'ready',
	once: true,
	async execute(client) {
		console.log(clc.green('Logged in!'));
		// Set our afk status to true
		client.user.setAFK(true);
	},
};

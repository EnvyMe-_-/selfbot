const {prefix} = require('../config.json');

module.exports = {
	name: 'messageCreate',
	async execute(client, message) {
		if (!message.content.startsWith(prefix) || message.author.id !== client.user.id) {
			return;
		}

		const args = message.content.slice(prefix.length).trim().split(/ +/);
		const command = args.shift().toLowerCase();

		if (!client.commands.has(command)) {
			return;
		}

		try {
			message.delete();
			client.commands.get(command).execute(message, args);
		} catch (error) {
			console.error(error);
			message.reply('there was an error trying to execute that command!');
		}
	},
};

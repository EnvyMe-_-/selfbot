
module.exports = {
	name: 'ftoc',
	description: 'converts value given to Celsius',
	usage: '[value]',
	async execute(message, args) {
		if (args[0] === undefined) {
			return message.channel.send('Please provide a value to convert!');
		}

		if (args.length > 1) {
			return message.channel.send('Please only provide one value!');
		}

		const fahrenheit = args[0];
		// Round to 2 decimal places
		const celsius = Math.round(((fahrenheit - 32) * 5 / 9) * 100) / 100;
		message.channel.send(`${fahrenheit}°F is ${celsius}°C`);
	},
};

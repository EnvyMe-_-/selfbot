module.exports = {
	name: 'reload',
	description: 'reloads a command',
	usage: '[command]',
	execute(message, args) {
		// Make a command that reloads the module specified, if there is no module specified, reload all modules
		if (!args.length) {
			return message.channel.send('You didn\'t pass any command to reload!');
		}

		const commandName = args[0].toLowerCase();
		const command = message.client.commands.get(commandName);

		if (!command) {
			return message.channel.send('That\'s not a valid command!');
		}

		delete require.cache[require.resolve(`./${commandName}.js`)];

		try {
			const newCommand = require(`./${commandName}.js`);
			message.client.commands.set(newCommand.name, newCommand);
			message.channel.send(`Command \`${commandName}\` was reloaded!`);
		} catch (error) {
			console.error(error);
			message.channel.send(`There was an error while reloading a command \`${commandName}\`:\n\`${error.message}\``);
		}
	},

};

const CryptoHandler = require('../handlers/cryptoPricingHandler');
const cryptoHandler = new CryptoHandler();

module.exports = {
	name: 'crypto',
	description: 'grabs the current price of a crypto',
	usage: '[crypto non abbreviated] [currency]',
	async execute(message, args) {
		await cryptoHandler.getCryptoPrice(args[0], args[1]).then(cryptoPrice => {
			if (cryptoPrice === undefined) {
				message.channel.send('Error getting crypto price!');
			}

			message.channel.send(`The current price of ${args[0]} is ${cryptoPrice[args[0]][args[1]]} ${args[1].toUpperCase()}`);
		}).catch(error => {
			console.log(error);
		});
	},
};

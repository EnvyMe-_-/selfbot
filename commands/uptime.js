
module.exports = {
	name: 'uptime',
	description: 'grabs the uptime of the bot',
	usage: '',
	async execute(message) {
		const {uptime} = message.client;
		// Stolen maths (https://maah.gitbooks.io/discord-bots/content/examples/ping-and-uptime.html)
		const days = Math.floor(uptime / 86400000);
		const hours = Math.floor(uptime / 3600000) % 24;
		const minutes = Math.floor(uptime / 60000) % 60;
		const seconds = Math.floor(uptime / 1000) % 60;

		message.channel.send(`The bot has been up for: ${days}d ${hours}h ${minutes}m ${seconds}s`);
	},
};

module.exports = {
	name: 'load',
	description: 'loads new commands',
	usage: '[command]',
	execute(message, args) {
		// Make a command that loads the module specified, if there is no module specified, load all modules
		if (!args.length) {
			return message.channel.send('You didn\'t pass any command to load!');
		}

		const commandName = args[0].toLowerCase();
		const command = message.client.commands.get(commandName);

		if (command) {
			return message.channel.send('That command is already loaded!');
		}

		try {
			const newCommand = require(`./${commandName}.js`);
			message.client.commands.set(newCommand.name, newCommand);
			message.channel.send(`Command \`${commandName}\` was loaded!`);
		} catch (error) {
			console.error(error);
			message.channel.send(`There was an error while loading a command \`${commandName}\`:\n\`${error.message}\``);
		}
	},
};

module.exports = {
	name: 'status',
	description: 'sets the status of the bot',
	usage: '[type] [name] [status]',
	async execute(message, args) {
		// Get users activity
		const activity = message.client.user.presence.activities[0];

		if (activity && !args.length) {
			message.client.user.setActivity(null);
			message.client.user.setAFK(true);
			return message.channel.send('Status reset!');
		}

		if (!activity && !args.length) {
			return message.channel.send('Please provide a status to set!');
		}

		if (args.length < 3) {
			return message.channel.send('Please provide a name, status, and type!');
		}

		const statusName = args.slice(1, args.length - 1).join(' ');

		await message.client.user.setPresence({
			activities: [{
				name: statusName,
				type: args[0],
			}],
			status: args[2],
			afk: true,
		});
	},
};

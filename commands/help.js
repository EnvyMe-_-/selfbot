const {prefix} = require('../config.json');
module.exports = {
	name: 'help',
	description: 'Shows this command',
	usage: '',
	async execute(message, args) {
		// Make a help command that goes through all the available commands and descriptions, then output them into a nicely formatted message
		const data = [];
		const {commands} = message.client;

		// If there are no arguments, output all the commands and descriptions
		if (!args.length) {
			data.push('Here\'s a list of all my commands:');
			data.push(commands.map(command => command.name).join(', '));
			data.push(`You can send \`${prefix}help [command name]\` to get info on a specific command!`);

			return message.channel.send(data.join('\n'));
		}

		// If there are arguments, output the command name and description
		if (args.length) {
			const name = args[0].toLowerCase();
			const command = commands.get(name);

			if (!command) {
				return message.channel.send('That\'s not a valid command!');
			}

			data.push(`**Name:** ${command.name}`);

			if (command.description) {
				data.push(`**Description:** ${command.description}`);
			}

			if (command.usage) {
				data.push(`**Usage:** ${prefix}${command.name} ${command.usage}`);
			}

			// Join the data array into one string and send it
			return message.channel.send(data.join('\n'));
		}
	},
};

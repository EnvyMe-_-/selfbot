const SpotifyHandler = require('../handlers/spotifyHandler');
const spotifyHandler = new SpotifyHandler();
const {WebEmbed} = require('discord.js-selfbot-v13');

module.exports = {
	name: 'spotify',
	description: 'grabs our current spotify song, along with the link to it',
	usage: '',
	async execute(message) {
		await spotifyHandler.getCurrentSongInfo().then(songInfo => {
			if (songInfo === 'Nothing is playing!') {
				message.channel.send('Nothing is playing!');
			}

			if (songInfo.item === undefined) {
				return message.channel.send('Error getting song info!');
			}

			const w = new WebEmbed({
				shorten: true,
				hidden: false, // If you send this embed with MessagePayload.options.embeds, it must set to false
			})
				.setAuthor({name: `${songInfo.item.name} by ${songInfo.item.artists[0].name}`, url: `${songInfo.item.external_urls.spotify}`})
				.setColor('#1DB954')
				.setProvider({name: 'Currently playing on Spotify'})
				.setImage(songInfo.item.album.images[0].url);

			message.channel.send({embeds: [w]});
		}).catch(error => {
			console.log(error);
		});
	},
};
